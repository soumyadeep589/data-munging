datContent = [i.strip().split() for i in open("./football.dat").readlines()]

football_list = []
for x in datContent:
    if (len(x) == len(datContent[1])):
        football_list.append(x)
# print(football_list)

min_diff = abs(int(football_list[0][6]) - int(football_list[0][8]))
min_diff_team = football_list[0][1]
for each_team in football_list:
    if abs(int(each_team[6])-int(each_team[8]))<min_diff:
        min_diff = abs(int(each_team[6])-(int(each_team[8])))
        min_diff_team = each_team[1]
print(min_diff, min_diff_team)