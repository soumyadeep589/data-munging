datContent = [i.strip().split() for i in open("./weather.dat").readlines()]

weather_list = datContent[2:]
min_spread = abs(float(weather_list[0][1])- float(weather_list[0][2]))
min_spread_day = weather_list[0][0]
for each_day in weather_list:
    each_day[1] = each_day[1].split('*')[0] 
    each_day[2] = each_day[2].split('*')[0] 
    
    if abs(float(each_day[1])-float(each_day[2]))<min_spread:
        min_spread = abs(float(each_day[1])-float(each_day[2]))
        min_spread_day = each_day[0]
print(min_spread_day)